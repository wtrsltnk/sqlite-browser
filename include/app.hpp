#ifndef APP_H
#define APP_H

#include "loadeddatabase.hpp"

#include <string>
#include <vector>

class Query
{
    static LoadedDatabase _emptyDb;

public:
    Query();

    Query(
        LoadedDatabase &db);

    Query(
        const Query &query);

    LoadedDatabase &_db;
    std::string _title;
    std::string _query;
};

class App
{
public:
    App(const std::vector<std::string> &args);
    virtual ~App();

    bool Init();
    int Run();

    void OnInit();
    void OnFrame();
    void OnResize(int width, int height);
    void OnExit();

    template <class T>
    T *GetWindowHandle() const;

    void ObjectExplorer();
    void ObjectEditor();

protected:
    const std::vector<std::string> &_args;
    int _width = 1024;
    int _height = 768;
    std::vector<std::unique_ptr<LoadedDatabase>> _loadedDatabases;
    std::vector<Query> _openQueries;

    template <class T>
    void SetWindowHandle(T *handle);

    void ClearWindowHandle();

    void LoadDatabase(const std::filesystem::path &file);

    void Database(
        std::unique_ptr<LoadedDatabase> &db);

    void Table(
        std::unique_ptr<LoadedDatabase::Table> &table);

    void ScriptTableAsSelect(
        std::unique_ptr<LoadedDatabase::Table> &table);

private:
    void *_windowHandle;
};

#endif // APP_H
