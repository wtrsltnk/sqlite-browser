//
//  sqlitelib.h
//
//  Copyright (c) 2013 Yuji Hirose. All rights reserved.
//  The Boost Software License 1.0
//

#ifndef _CPPGENERICSQLITELIB_H_
#define _CPPGENERICSQLITELIB_H_

#include <sqlite3.h>

#include <string>
#include <tuple>
#include <type_traits>
#include <vector>

namespace genericsqlitelib
{
    namespace
    {

        void *enabler;

        inline void verify(int rc, int expected = SQLITE_OK)
        {
            if (rc != expected)
            {
                throw;
            }
        }

        template <typename T>
        T get_column_value(sqlite3_stmt *stmt, int col) {}

        template <>
        int get_column_value<int>(sqlite3_stmt *stmt, int col)
        {
            return sqlite3_column_int(stmt, col);
        }

        template <>
        double get_column_value<double>(sqlite3_stmt *stmt, int col)
        {
            return sqlite3_column_double(stmt, col);
        }

        template <>
        std::string get_column_value<std::string>(sqlite3_stmt *stmt, int col)
        {
            auto val = std::string(sqlite3_column_bytes(stmt, col), 0);
            memcpy(&val[0], sqlite3_column_text(stmt, col), val.size());
            return val;
        }

        template <>
        std::vector<char> get_column_value<std::vector<char>>(sqlite3_stmt *stmt,
                                                              int col)
        {
            auto val = std::vector<char>(sqlite3_column_bytes(stmt, col));
            memcpy(&val[0], sqlite3_column_blob(stmt, col), val.size());
            return val;
        }

        template <int N, typename T, typename... Rest>
        struct ColumnValues;

        template <int N, typename T, typename... Rest>
        struct ColumnValues
        {
            static std::tuple<T, Rest...> get(sqlite3_stmt *stmt, int col)
            {
                return std::tuple_cat(std::make_tuple(get_column_value<T>(stmt, col)), ColumnValues<N - 1, Rest...>::get(stmt, col + 1));
            }
        };

        template <typename T>
        struct ColumnValues<1, T>
        {
            static std::tuple<T> get(sqlite3_stmt *stmt, int col)
            {
                return std::make_tuple(get_column_value<T>(stmt, col));
            }
        };

        template <typename Arg>
        void bind_value(sqlite3_stmt *stmt, int col, Arg val) {}

        template <>
        void bind_value<int>(sqlite3_stmt *stmt, int col, int val)
        {
            verify(sqlite3_bind_int(stmt, col, val));
        }

        template <>
        void bind_value<double>(sqlite3_stmt *stmt, int col, double val)
        {
            verify(sqlite3_bind_double(stmt, col, val));
        }

        template <>
        void bind_value<std::string>(sqlite3_stmt *stmt, int col, std::string val)
        {
            verify(sqlite3_bind_text(stmt, col, val.data(), val.size(), SQLITE_TRANSIENT));
        }

        template <>
        void bind_value<const char *>(sqlite3_stmt *stmt, int col, const char *val)
        {
            verify(sqlite3_bind_text(stmt, col, val, strlen(val), SQLITE_TRANSIENT));
        }

        template <>
        void bind_value<std::vector<char>>(sqlite3_stmt *stmt, int col, std::vector<char> val)
        {
            verify(sqlite3_bind_blob(stmt, col, val.data(), val.size(), SQLITE_TRANSIENT));
        }

    }; // namespace

    class RowType
    {
    public:
        RowType(sqlite3_stmt *stmt, int id) : stmt_(stmt), id_(id) {}

    private:
        sqlite3_stmt *stmt_;
        int id_;
    };

    class Iterator : public std::iterator<std::forward_iterator_tag, RowType>
    {
    public:
        Iterator() : stmt_(nullptr), id_(-1) {}

        Iterator(sqlite3_stmt *stmt) : stmt_(stmt), id_(-1) { operator++(); }

        RowType operator*() const
        {
            return RowType(stmt_, id_);
        }

        Iterator &operator++()
        {
            if (stmt_)
            {
                auto rc = sqlite3_step(stmt_);
                if (rc == SQLITE_ROW)
                {
                    ++id_;
                }
                else if (rc == SQLITE_DONE)
                {
                    id_ = -1;
                }
                else
                {
                    throw; // TODO:
                }
            }
            else
            {
                throw; // TODO:
            }
            return *this;
        }

        bool operator==(const Iterator &rhs) { return id_ == rhs.id_; }

        bool operator!=(const Iterator &rhs) { return !operator==(rhs); }

    private:
        sqlite3_stmt *stmt_;
        int id_;
    };

    class Cursor
    {
    public:
        Cursor() = delete;
        Cursor(const Cursor &) = delete;
        Cursor &operator=(const Cursor &) = delete;

        Cursor(Cursor &&rhs) : stmt_(rhs.stmt_) {}

        Cursor(sqlite3_stmt *stmt) : stmt_(stmt) {}

        Iterator begin() { return Iterator(stmt_); }

        Iterator end() { return Iterator(); }

    private:
        sqlite3_stmt *stmt_;
    };

    class Statement
    {
    public:
        Statement(sqlite3 *db, const std::string &query) : stmt_(nullptr)
        {
            verify(sqlite3_prepare(db, query.c_str(), query.size(), &stmt_, nullptr));
        }

        Statement(Statement &&rhs) : stmt_(rhs.stmt_) { rhs.stmt_ = nullptr; }

        ~Statement() { verify(sqlite3_finalize(stmt_)); }

        template <typename... Args>
        Statement &bind(const Args &... args)
        {
            verify(sqlite3_reset(stmt_));
            bind_values(1, args...);
            return *this;
        }

        template <typename... Args>
        Cursor execute_cursor(const Args &... args)
        {
            bind(args...);
            return Cursor(stmt_);
        }

    private:
        Statement(const Statement &rhs);
        Statement &operator=(const Statement &rhs);

        sqlite3_stmt *stmt_;

        inline void bind_values(int col) {}

        template <typename Arg, typename... ArgRest>
        void bind_values(int col, const Arg &val, const ArgRest &... rest)
        {
            bind_value(stmt_, col, val);
            bind_values(col + 1, rest...);
        }
    };

    class GenericSqlite
    {
    public:
        GenericSqlite() = delete;
        GenericSqlite(const GenericSqlite &) = delete;
        GenericSqlite &operator=(const GenericSqlite &) = delete;

        GenericSqlite(sqlite3 *db) : db_(db) {}

        ~GenericSqlite() {}

        bool is_open() const { return db_ != nullptr; }

        Statement prepare(const char *query) const
        {
            return Statement(db_, query);
        }

        template <typename... Args>
        Cursor execute_cursor(const char *query, const Args &... args)
        {
            return prepare(query).execute_cursor(args...);
        }

    private:
        sqlite3 *db_;
    };

} // namespace genericsqlitelib

#endif
