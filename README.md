# Sqlite.Browser

Sqlite browser using Glfw project with IMGUI and the latest OpenGL.

This project makes use of the following libraries/projects:

* [CPP-SQLITELIB](https://github.com/yhirose/cpp-sqlitelib)
* [GLFW](https://www.glfw.org/)
* [IMGUI](https://github.com/ocornut/imgui/)
* [CPM](https://github.com/cpm-cmake/CPM.cmake)
* [GLAD](https://glad.dav1d.de/)

![Screenshot](screenshot.png)
