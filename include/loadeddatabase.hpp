#ifndef LOADEDDATABASE_H
#define LOADEDDATABASE_H

#include <filesystem>
#include <memory>
#include <sqlitelib.h>
#include <string>
#include <vector>

class LoadedDatabase
{
public:
    class Table
    {
    public:
        class Column
        {
        public:
            Column(std::unique_ptr<Table> &parentTable);

            std::string _name;
            int _size;
            std::string _type;

        private:
            std::unique_ptr<Table> &_parentTable;
        };

    public:
        Table(LoadedDatabase &parentDatabase);

        std::string _name;
        std::vector<std::unique_ptr<Column>> _columns;
        size_t _columnCount = 0;

        LoadedDatabase &GetParentDatabase();

    private:
        LoadedDatabase &_parentDatabase;
    };

    class View
    {
    public:
        View(LoadedDatabase &parentDatabase);

        std::string _name;

    private:
        LoadedDatabase &_parentDatabase;
    };

    class Index
    {
    public:
        Index(LoadedDatabase &parentDatabase);

        std::string _name;

    private:
        LoadedDatabase &_parentDatabase;
    };

    class Trigger
    {
    public:
        Trigger(LoadedDatabase &parentDatabase);

        std::string _name;

    private:
        LoadedDatabase &_parentDatabase;
    };

public:
    LoadedDatabase();

    void Load();
    void LoadTables();
    void LoadTableColumns(
        std::unique_ptr<LoadedDatabase::Table> &table);
    void LoadViews();
    void LoadIndices();
    void LoadTriggers();
    void ExecuteQuery(
        const std::string &query);

    std::filesystem::path _filePath;
    std::unique_ptr<sqlitelib::Sqlite> _sqlitedb;
    std::vector<std::unique_ptr<Table>> _tables;
    int _tableCount = 0;
    std::vector<std::unique_ptr<View>> _views;
    int _viewCount = 0;
    std::vector<std::unique_ptr<Index>> _indices;
    int _indexCount = 0;
    std::vector<std::unique_ptr<Trigger>> _triggers;
    int _triggerCount = 0;

private:
};

#endif // LOADEDDATABASE_H
