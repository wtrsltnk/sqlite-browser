#include "loadeddatabase.hpp"
#include <memory>

#include <genericsqlitelib.h>

LoadedDatabase::LoadedDatabase()
{}

LoadedDatabase::Table::Column::Column(
    std::unique_ptr<LoadedDatabase::Table> &parentTable)
    : _parentTable(parentTable)
{}

LoadedDatabase::Table::Table(LoadedDatabase &parentDatabase)
    : _parentDatabase(parentDatabase)
{}

LoadedDatabase &LoadedDatabase::Table::GetParentDatabase()
{
    return _parentDatabase;
}

LoadedDatabase::View::View(LoadedDatabase &parentDatabase)
    : _parentDatabase(parentDatabase)
{}

LoadedDatabase::Index::Index(LoadedDatabase &parentDatabase)
    : _parentDatabase(parentDatabase)
{}

LoadedDatabase::Trigger::Trigger(LoadedDatabase &parentDatabase)
    : _parentDatabase(parentDatabase)
{}

void LoadedDatabase::Load()
{
    _sqlitedb = std::make_unique<sqlitelib::Sqlite>(_filePath.string().c_str());
    _tableCount = _sqlitedb->execute_value<int>("SELECT COUNT(name) FROM sqlite_master WHERE type='table';");
    _viewCount = _sqlitedb->execute_value<int>("SELECT COUNT(name) FROM sqlite_master WHERE type='view';");
    _indexCount = _sqlitedb->execute_value<int>("SELECT COUNT(name) FROM sqlite_master WHERE type='index';");
    _triggerCount = _sqlitedb->execute_value<int>("SELECT COUNT(name) FROM sqlite_master WHERE type='trigger';");
}

void LoadedDatabase::LoadTableColumns(
    std::unique_ptr<LoadedDatabase::Table> &table)
{
    table->_columns.clear();

    std::stringstream ss;

    ss << "PRAGMA table_info('" + table->_name + "');";

    auto rows = _sqlitedb->execute<int, std::string, std::string, int, std::string, int>(ss.str().c_str());

    table->_columnCount = rows.size();

    for (const auto &name : rows)
    {
        auto item = std::make_unique<LoadedDatabase::Table::Column>(table);
        item->_name = std::get<1>(name);
        table->_columns.push_back(std::move(item));
    }
}

void LoadedDatabase::LoadTables()
{
    _tables.clear();

    auto stmt = _sqlitedb->prepare<std::string>("SELECT name FROM sqlite_master WHERE type='table';");

    for (const auto &name : stmt.execute_cursor())
    {
        auto item = std::make_unique<LoadedDatabase::Table>(*this);
        item->_name = name;
        _tables.push_back(std::move(item));
    }
}

void LoadedDatabase::LoadViews()
{
    _views.clear();

    auto stmt = _sqlitedb->prepare<std::string>("SELECT name FROM sqlite_master WHERE type='view';");

    for (const auto &name : stmt.execute_cursor())
    {
        auto item = std::make_unique<LoadedDatabase::View>(*this);
        item->_name = name;
        _views.push_back(std::move(item));
    }
}

void LoadedDatabase::LoadIndices()
{
    _indices.clear();

    auto stmt = _sqlitedb->prepare<std::string>("SELECT name FROM sqlite_master WHERE type='index';");

    for (const auto &name : stmt.execute_cursor())
    {
        auto item = std::make_unique<LoadedDatabase::Index>(*this);
        item->_name = name;
        _indices.push_back(std::move(item));
    }
}

void LoadedDatabase::LoadTriggers()
{
    _triggers.clear();

    auto stmt = _sqlitedb->prepare<std::string>("SELECT name FROM sqlite_master WHERE type='trigger';");

    for (const auto &name : stmt.execute_cursor())
    {
        auto item = std::make_unique<LoadedDatabase::Trigger>(*this);
        item->_name = name;
        _triggers.push_back(std::move(item));
    }
}

void LoadedDatabase::ExecuteQuery(
    const std::string &query)
{
    genericsqlitelib::GenericSqlite gendb(_sqlitedb->get_db());

    auto result_cursor = gendb.execute_cursor(query.c_str());

    for (auto row : result_cursor)
    {
    }
}
