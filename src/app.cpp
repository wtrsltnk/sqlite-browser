#include <app.hpp>
#include <glad/glad.h>

#include <imgui.h>
#include <imgui_internal.h>

#include <imfilebrowser.h>

LoadedDatabase Query::_emptyDb;

Query::Query()
    : _db(_emptyDb)
{}

Query::Query(
    LoadedDatabase &db)
    : _db(db)
{}

Query::Query(
    const Query &query)
    : _db(query._db),
      _title(query._title),
      _query(query._query)
{}

void App::OnInit()
{
    glClearColor(0.56f, 0.7f, 0.67f, 1.0f);

    if (!_args.empty())
    {
        LoadDatabase(_args.front());
    }
}

void App::OnResize(int width, int height)
{
    _width = width;
    _height = height;

    glViewport(0, 0, width, height);
}
void DrawSplitter(int split_vertically, float thickness, float *size0, float *size1, float min_size0, float min_size1)
{
    ImVec2 backup_pos = ImGui::GetCursorPos();
    if (split_vertically)
        ImGui::SetCursorPosY(backup_pos.y + *size0);
    else
        ImGui::SetCursorPosX(backup_pos.x + *size0);

    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0, 0, 0, 0)); // We don't draw while active/pressed because as we move the panes the splitter button will be 1 frame late
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.6f, 0.6f, 0.6f, 0.10f));
    ImGui::Button("##Splitter", ImVec2(!split_vertically ? thickness : -1.0f, split_vertically ? thickness : -1.0f));
    ImGui::PopStyleColor(3);

    ImGui::SetItemAllowOverlap(); // This is to allow having other buttons OVER our splitter.

    if (ImGui::IsItemActive())
    {
        float mouse_delta = split_vertically ? ImGui::GetIO().MouseDelta.y : ImGui::GetIO().MouseDelta.x;

        // Minimum pane size
        if (mouse_delta < min_size0 - *size0)
            mouse_delta = min_size0 - *size0;
        if (mouse_delta > *size1 - min_size1)
            mouse_delta = *size1 - min_size1;

        // Apply resize
        *size0 += mouse_delta;
        *size1 -= mouse_delta;
    }
    ImGui::SetCursorPos(backup_pos);
}
void App::OnFrame()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ImVec2 menuSize;
    if (ImGui::BeginMainMenuBar())
    {
        menuSize = ImGui::GetWindowSize();

        if (ImGui::BeginMenu("File"))
        {
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Edit"))
        {
            if (ImGui::MenuItem("Undo", "CTRL+Z"))
            {
            }
            if (ImGui::MenuItem("Redo", "CTRL+Y", false, false))
            {
            } // Disabled item
            ImGui::Separator();
            if (ImGui::MenuItem("Cut", "CTRL+X"))
            {
            }
            if (ImGui::MenuItem("Copy", "CTRL+C"))
            {
            }
            if (ImGui::MenuItem("Paste", "CTRL+V"))
            {
            }
            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

    ImGui::SetNextWindowPos(ImVec2(0, menuSize.y));
    ImGui::SetNextWindowSize(ImVec2(float(_width), float(_height) - menuSize.y));

    ImGui::Begin("Main", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize);

    static float size0 = float(_width) * 0.4f, size1 = float(_width) * 0.6f;

    DrawSplitter(0, 8.0f, &size0, &size1, 100, 100);

    ImGui::BeginChild("ObjectExplorer", ImVec2(size0, 0), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize);

    ObjectExplorer();

    ImGui::EndChild();

    ImGui::SameLine();

    ImGui::BeginChild("ObjectEditor", ImVec2(float(_width) - size0 - 28, 0), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize);

    ObjectEditor();

    ImGui::EndChild();

    ImGui::End();
}

void LoadTableColumns(
    std::unique_ptr<LoadedDatabase::Table> &table)
{
    table->GetParentDatabase().LoadTableColumns(table);
}

void App::ScriptTableAsSelect(
    std::unique_ptr<LoadedDatabase::Table> &table)
{
    if (table->_columns.empty())
    {
        LoadTableColumns(table);
    }

    std::stringstream query;

    query << "SELECT TOP 1000 ";

    for (auto &column : table->_columns)
    {
        query << "\n\t" << column->_name;
        if (column != table->_columns.back())
        {
            query << ",";
        }
    }

    query << "\nFROM " << table->_name << ";";

    Query qobj(table->GetParentDatabase());
    qobj._query = query.str();
    std::stringstream titlestream;
    titlestream << table->_name << "-" << std::setfill('0') << std::setw(3) << (_openQueries.size() + 1);
    qobj._title = titlestream.str();
    _openQueries.push_back(qobj);
}

void App::Table(
    std::unique_ptr<LoadedDatabase::Table> &table)
{
    ImGui::PushID(table->_name.c_str());

    auto single_table = ImGui::TreeNode(table->_name.c_str());
    if (ImGui::BeginPopupContextItem("Single Table context menu"))
    {
        if (ImGui::MenuItem("Select Top 1000 Rows"))
        {
            ScriptTableAsSelect(table);
        }
        ImGui::MenuItem("Edit Top 200 Rows");
        if (ImGui::BeginMenu("Script Table as"))
        {
            ImGui::MenuItem("CREATE");
            ImGui::MenuItem("DROP");
            ImGui::MenuItem("DROP And CREATE");
            ImGui::Separator();
            if (ImGui::MenuItem("SELECT"))
            {
                ScriptTableAsSelect(table);
            }
            ImGui::MenuItem("INSERT");
            ImGui::MenuItem("UPDATE");
            ImGui::MenuItem("DELETE");
            ImGui::EndMenu();
        }
        ImGui::Separator();
        ImGui::MenuItem("Refresh");
        ImGui::EndPopup();
    }
    if (single_table)
    {
        if (table->_columns.empty())
        {
            LoadTableColumns(table);
        }

        auto columns = ImGui::TreeNode("Columns");
        if (ImGui::BeginPopupContextItem("Columns context menu"))
        {
            ImGui::MenuItem("New Column...");
            ImGui::Separator();
            ImGui::MenuItem("Refresh");
            ImGui::EndPopup();
        }
        if (columns)
        {
            for (auto &column : table->_columns)
            {
                ImGui::TreeNodeEx(column->_name.c_str(), ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen);
                if (ImGui::BeginPopupContextItem("Column context menu"))
                {
                    ImGui::MenuItem("Modify");
                    ImGui::Separator();
                    ImGui::MenuItem("Refresh");
                    ImGui::EndPopup();
                }
            }
            ImGui::TreePop();
        }

        auto keys = ImGui::TreeNode("Keys");
        if (ImGui::BeginPopupContextItem("Columns context menu"))
        {
            ImGui::MenuItem("New Column...");
            ImGui::Separator();
            ImGui::MenuItem("Refresh");
            ImGui::EndPopup();
        }
        if (keys)
        {
            ImGui::TreeNodeEx("Key", ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen);
            if (ImGui::BeginPopupContextItem("Key context menu"))
            {
                ImGui::MenuItem("Modify");
                ImGui::Separator();
                ImGui::MenuItem("Refresh");
                ImGui::EndPopup();
            }
            ImGui::TreePop();
        }

        auto constraints = ImGui::TreeNode("Constraints");
        if (ImGui::BeginPopupContextItem("Constraints context menu"))
        {
            ImGui::MenuItem("New Constraint...");
            ImGui::Separator();
            ImGui::MenuItem("Refresh");
            ImGui::EndPopup();
        }
        if (constraints)
        {
            ImGui::TreeNodeEx("Constraint", ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen);
            if (ImGui::BeginPopupContextItem("Constraint context menu"))
            {
                ImGui::MenuItem("Modify");
                ImGui::Separator();
                ImGui::MenuItem("Refresh");
                ImGui::EndPopup();
            }
            ImGui::TreePop();
        }
        ImGui::TreePop();
    }

    ImGui::PopID();
}

void LoadDatabaseTables(
    std::unique_ptr<LoadedDatabase> &db)
{
    db->LoadTables();
    db->LoadViews();
    db->LoadIndices();
    db->LoadTriggers();
}

void App::Database(
    std::unique_ptr<LoadedDatabase> &db)
{
    ImGui::PushID(db->_filePath.filename().string().c_str());

    auto db1 = ImGui::CollapsingHeader(db->_filePath.string().c_str());
    if (ImGui::BeginPopupContextItem("Database context menu"))
    {
        ImGui::MenuItem("Disconnect");
        ImGui::Separator();
        ImGui::MenuItem("New Query");
        if (ImGui::BeginMenu("Script Database as"))
        {
            ImGui::MenuItem("CREATE");
            ImGui::MenuItem("DROP");
            ImGui::MenuItem("DROP And CREATE");
            ImGui::EndMenu();
        }
        ImGui::Separator();
        if (ImGui::BeginMenu("Tasks"))
        {
            ImGui::MenuItem("Vacuum");
            ImGui::MenuItem("Analyze");
            ImGui::MenuItem("Integrity Check");
            ImGui::Separator();
            ImGui::MenuItem("Import Data...");
            ImGui::MenuItem("Export Data...");
            ImGui::EndMenu();
        }
        ImGui::Separator();
        ImGui::MenuItem("Refresh");
        ImGui::MenuItem("Properties");
        ImGui::EndPopup();
    }
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(450.0f);
        ImGui::TextUnformatted(db->_filePath.string().c_str());
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
    if (db1)
    {
        if (db->_tables.empty() && db->_tableCount > 0)
        {
            LoadDatabaseTables(db);
        }

        auto tables = ImGui::TreeNode("Tables", "Tables (%d)", db->_tableCount);
        if (ImGui::BeginPopupContextItem("Tables context menu"))
        {
            ImGui::MenuItem("New Table...");
            ImGui::Separator();
            ImGui::MenuItem("Refresh");
            ImGui::EndPopup();
        }
        if (tables)
        {
            for (auto &table : db->_tables)
            {
                Table(table);
            }
            ImGui::TreePop();
        }

        if (ImGui::TreeNode("Views", "Views (%d)", db->_viewCount))
        {
            for (auto &view : db->_views)
            {
                ImGui::TreeNodeEx(view->_name.c_str(), ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen);
            }
            ImGui::TreePop();
        }
        if (ImGui::TreeNode("Indices", "Indices (%d)", db->_indexCount))
        {
            for (auto &index : db->_indices)
            {
                ImGui::TreeNodeEx(index->_name.c_str(), ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen);
            }
            ImGui::TreePop();
        }
        if (ImGui::TreeNode("Triggers", "Triggers (%d)", db->_triggerCount))
        {
            for (auto &trigger : db->_triggers)
            {
                ImGui::TreeNodeEx(trigger->_name.c_str(), ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen);
            }
            ImGui::TreePop();
        }
    }

    ImGui::PopID();
}

void App::LoadDatabase(const std::filesystem::path &file)
{
    auto db = new LoadedDatabase();
    db->_filePath = file;

    db->Load();

    _loadedDatabases.push_back(std::unique_ptr<LoadedDatabase>(db));
}

void App::ObjectExplorer()
{
    static ImGui::FileBrowser fileDialog;
    fileDialog.SetTitle("title");
    fileDialog.SetTypeFilters({".sqlite", ".db"});

    //ImGui::Spacing();
    ImGui::BeginGroup();
    if (ImGui::Button("Connect"))
    {
        fileDialog.Open();
    }
    ImGui::SameLine();
    if (ImGui::Button("Refresh"))
    {
    }
    ImGui::EndGroup();

    fileDialog.Display();

    if (fileDialog.HasSelected())
    {
        LoadDatabase(fileDialog.GetSelected());

        fileDialog.ClearSelected();
    }

    for (auto &db : _loadedDatabases)
    {
        Database(db);
    }
}

void App::ObjectEditor()
{
    static int selectedObject = 0;
    int i = 0;
    for (auto &q : _openQueries)
    {
        if (i != 0)
        {
            ImGui::SameLine();
        }

        if (ImGui::RadioButton(q._title.c_str(), selectedObject == i))
        {
            selectedObject = i;
        }

        i++;
    }

    if (_openQueries.empty())
    {
        return;
    }

    ImGui::PushItemWidth(-1);
    char textBuffer[1024];
    strcpy_s(textBuffer, 1024, _openQueries[selectedObject]._query.c_str());
    ImGui::InputTextEx("##sql", "", textBuffer, 1024, ImVec2(0, 300), ImGuiInputTextFlags_Multiline);
    ImGui::PopItemWidth();
    if (ImGui::Button("Execute"))
    {
        _openQueries[selectedObject]._db.ExecuteQuery(_openQueries[selectedObject]._query);
    }
}

void App::OnExit()
{
}
